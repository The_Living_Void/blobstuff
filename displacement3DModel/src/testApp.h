#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"

#include "ofx3DModelLoader.h"
#include "MyEquations.h"

class testApp : public ofBaseApp{
public:

	void setup();
	void update();
	void draw();
    void updateMidColor();
    
    ofColor midColor;

    // ofLight light;
    Parabala parabala;
    Torus torus;
    Mobius mobius;
    Tube tube;
    Spiral spriral;
    Thing thing;
    Steiner steiner;
    Boys boys;
    Breather breather;
    
    ofVboMesh mesh;
    
	ofImage colormap,bumpmap;
	ofShader shader;
	
    // camera
    //ofCamera cam; // add mouse controls for camera movement
	ofVideoGrabber vidGrabber;
    
    //anti aliasing
    ofFbo fbo;
    ofFbo texFbo;
    ofFbo squirrelFbo;
    
    ofx3DModelLoader squirrel;
    
    
    
    
};

#endif

