uniform sampler2D colormap;
uniform sampler2D bumpmap;
varying vec2  TexCoord;
uniform float maxHeight;
uniform float dis;

void main(void) {
	TexCoord = gl_MultiTexCoord0.st;

	vec4 bumpColor = texture2D(bumpmap, TexCoord);
	float df = 0.30*bumpColor.r + 0.59*bumpColor.g + 0.11*bumpColor.b;
	vec4 newVertexPos = vec4(gl_Normal * df * maxHeight, 0.0) + gl_Vertex;

	gl_Position = (1.0-dis) * (gl_ModelViewProjectionMatrix * gl_Vertex) + dis * (gl_ModelViewProjectionMatrix * newVertexPos);
    gl_Position = (gl_ModelViewProjectionMatrix * newVertexPos);
} 